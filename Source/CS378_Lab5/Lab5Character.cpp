// Fill out your copyright notice in the Description page of Project Settings.


#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Lab5Character.h"

// Sets default values
ALab5Character::ALab5Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    // Create a camera boom...
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when ship does
    CameraBoom->TargetArmLength = 400.f;
    CameraBoom->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
    CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

    // Create a camera...
    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("OvertheShoulderCamera"));
    CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    CameraComponent->bUsePawnControlRotation = false;    // Camera does not rotate relative to arm
    

}

// Called when the game starts or when spawned
void ALab5Character::BeginPlay()
{
	Super::BeginPlay();
    isRunning = false;
    GetCharacterMovement()->MaxWalkSpeed = 100.0f;
	
}

// Called every frame
void ALab5Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALab5Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ALab5Character::MoveForward(float value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
    AddMovementInput(Direction, value);
}
void ALab5Character::MoveRight(float value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
    AddMovementInput(Direction, value);
}
void ALab5Character::JumpUp()
{
    this->Jump();
}
void ALab5Character::Interact()
{
    
}
bool ALab5Character::CanPerformAction(ECharacterActionStateEnum updatedAction)
{
    switch(CharacterActionState)
    {
        case ECharacterActionStateEnum::IDLE:
            return true;
            break;
        case ECharacterActionStateEnum::MOVE:
            if(updatedAction != ECharacterActionStateEnum::INTERACT)
            {
                return true;
            }
            break;
        case ECharacterActionStateEnum::JUMP:
            if (updatedAction == ECharacterActionStateEnum::IDLE || updatedAction == ECharacterActionStateEnum::MOVE)
            {
                return true;
            }
            break;
        case ECharacterActionStateEnum::INTERACT:
            return false;
    }
    return false;
}
void ALab5Character::BeginInteraction()
{
    if(GEngine)
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Begin Interaction"));
    }
    GetWorld()->GetTimerManager().SetTimer(InteractionTimerHandle, this, &ALab5Character::EndInteraction, 3.0f, false);
    
    
}
void ALab5Character::EndInteraction()
{
    if(GEngine)
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("End Interaction"));
    }
    UpdateActionState(ECharacterActionStateEnum::IDLE);
    
}
void ALab5Character::UpdateActionState(ECharacterActionStateEnum newAction)
{
    if (newAction == ECharacterActionStateEnum::MOVE || newAction == ECharacterActionStateEnum::IDLE)
    {
        if (FMath::Abs(GetVelocity().Size()) <= 0.01f)
        {
            CharacterActionState = ECharacterActionStateEnum::IDLE;
        }
        else
        {
            CharacterActionState = ECharacterActionStateEnum::MOVE;
            
        }
    }
    else
    {
        CharacterActionState = newAction;
    }
}

void ALab5Character::ShiftSpeed()
{
    if(isRunning)
    {
        GetCharacterMovement()->MaxWalkSpeed = 100.0f;
        isRunning = false;
    }
    else
    {
        GetCharacterMovement()->MaxWalkSpeed = 600.0f;
        isRunning = true;
    }
    
}

