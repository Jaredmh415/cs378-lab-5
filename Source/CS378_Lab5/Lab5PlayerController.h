// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Lab5PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB5_API ALab5PlayerController : public APlayerController
{
    GENERATED_BODY()
    
public:
    ALab5PlayerController();
    
    UFUNCTION()
    void JumpAction();
    UFUNCTION()
    void InteractAction();
    UFUNCTION()
    void ForwardMove(float value);
    UFUNCTION()
    void RightMove(float value);
    UFUNCTION()
    void EnableSpeed();
   
    
    
protected:
    
    virtual void SetupInputComponent() override;
    
};
