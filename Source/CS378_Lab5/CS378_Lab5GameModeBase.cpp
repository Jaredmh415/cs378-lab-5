// Copyright Epic Games, Inc. All Rights Reserved.


#include "Lab5Character.h"
#include "Lab5PlayerController.h"
#include "CS378_Lab5GameModeBase.h"
ACS378_Lab5GameModeBase::ACS378_Lab5GameModeBase()
{
    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Blueprint'/Game/Blueprints/Lab5CharacterBP.Lab5CharacterBP_C'"));
     if (pawnBPClass.Object)
    {
        if (GEngine)
        {
         GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Found"));
        }
        UClass* pawnBP = (UClass* )pawnBPClass.Object;
        DefaultPawnClass = pawnBP;
    }
    else
    {
        GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Not Found"));
        DefaultPawnClass = ALab5Character::StaticClass();
    }
    PlayerControllerClass = ALab5PlayerController::StaticClass();
    
    
}
