// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab5PlayerController.h"
#include "Lab5Character.h"

ALab5PlayerController::ALab5PlayerController()
{
    
}

void ALab5PlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();
    InputComponent->BindAction("JumpMovement", IE_Pressed, this, &ALab5PlayerController::JumpAction);
    InputComponent->BindAction("SpeedClick", IE_Pressed, this, &ALab5PlayerController::EnableSpeed);
    InputComponent->BindAction("InteractionKey", IE_Pressed, this, &ALab5PlayerController::InteractAction);
    InputComponent->BindAxis("ForwardMovement", this, &ALab5PlayerController::ForwardMove);
    InputComponent->BindAxis("RightMovement", this, &ALab5PlayerController::RightMove);
    
    
}
void ALab5PlayerController::InteractAction()
{
    ALab5Character * character = Cast<ALab5Character>(this->GetCharacter());
    if (character)
    {
        character->InteractPressed();
    }
}
void ALab5PlayerController::ForwardMove(float value)
{
//    if (GEngine)
//    {
//     GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Black, FString::Printf(TEXT("Forward Controller: %f"), value));
//    }
    ALab5Character * character = Cast<ALab5Character>(this->GetCharacter());
    if (character)
    {
        character->ForwardMovementAxis(value);
    }
}
void ALab5PlayerController::RightMove(float value)
{
    ALab5Character * character = Cast<ALab5Character>(this->GetCharacter());
    if (character)
    {
        character->RightMovementAxis(value);
    }
}
void ALab5PlayerController::JumpAction()
{
    ALab5Character * character = Cast<ALab5Character>(this->GetCharacter());
    if (character)
    {
        character->JumpMovementPressed();
    }
}
void ALab5PlayerController::EnableSpeed()
{
    ALab5Character * character = Cast<ALab5Character>(this->GetCharacter());
    if (character)
    {
        character->SpeedButton();
    }
    
}
