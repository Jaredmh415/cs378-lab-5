// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TimerManager.h"
#include "GameFramework/Character.h"
#include "Lab5Character.generated.h"

UENUM(BlueprintType)
enum class ECharacterActionStateEnum: uint8 {
    IDLE UMETA(DisplayName = "Idling"),
    MOVE UMETA(DisplayName = "Moving"),
    JUMP UMETA(DisplayName = "Jumping"),
    INTERACT UMETA(DisplayName = "Interacting")
};


UCLASS()
class CS378_LAB5_API ALab5Character : public ACharacter
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    ALab5Character();
    
    FORCEINLINE class UCameraComponent * GetCameraComponent() const { return CameraComponent;}
    FORCEINLINE class USpringArmComponent * GetCameraBoom() const {return CameraBoom; }

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    FTimerHandle InteractionTimerHandle;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    ECharacterActionStateEnum CharacterActionState;
    
    UPROPERTY(Category = Camera, EditAnywhere, BlueprintReadWrite)
    class UCameraComponent * CameraComponent;
    
    UPROPERTY(Category = Camera, EditAnywhere, BlueprintReadWrite)
    class USpringArmComponent * CameraBoom;
public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    
    UFUNCTION(BlueprintImplementableEvent)
    void InteractPressed();
    
    UFUNCTION(BlueprintImplementableEvent)
    void JumpMovementPressed();
    
    UFUNCTION(BlueprintImplementableEvent)
    void ForwardMovementAxis(float value);
    
    UFUNCTION(BlueprintImplementableEvent)
    void RightMovementAxis(float value);
    
    UFUNCTION(BlueprintImplementableEvent)
    void SpeedButton();
    
    UFUNCTION(BlueprintCallable)
    void ShiftSpeed();
    
    UFUNCTION(BlueprintCallable)
    void MoveForward(float value);
    
    UFUNCTION(BlueprintCallable)
    void MoveRight(float value);
    
    UFUNCTION(BlueprintCallable)
    void JumpUp();
    
    UFUNCTION(BlueprintCallable)
    void Interact();
    
    UFUNCTION(BlueprintCallable)
    bool CanPerformAction(ECharacterActionStateEnum updatedAction);
    
    UFUNCTION(BlueprintCallable)
    void BeginInteraction();
    
    UFUNCTION(BlueprintCallable)
    void EndInteraction();
    
    UFUNCTION(BlueprintCallable)
    void UpdateActionState(ECharacterActionStateEnum newAction);

    // Static names for axis bindings
    static const FName MoveForwardBinding;
    static const FName MoveRightBinding;
    
    //Static names for action bindings
    static const FName JumpBinding;
    static const FName InteractBinding;
    
    bool isRunning;

};

